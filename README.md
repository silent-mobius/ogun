# Ogun

__Ogun__ is a shell script based utility to manipulate Linux ISO files. Main purpose is to Customize Debian based ISO files in a manner that is required by user.

## Usage



## Installation

In current version no need to install the tool in any way.
So, just copy the shell script to working directory of your choice and use `./ogun -h` to learn how to use.
In future versions debian package will be generated.

#### Dependencies

These packages are required for tool to work. Use:

```sh
sudo ./ogun -d 
```
To satisfy the dependencies run the tool with `-d` argument and it will try to satisfy the dependencies.  Just in case the function is buggy, install the tools manually with list below.

- p7zip-full 
- git 
- genisoimage 
- fakeroot 
- pwgen 
- whois 
- xorriso 
- isolinux 
- binutils 
- squashfs-tools


#### Usage

After you have verified  the dependencies with `sudo ./ogun -d`, download live image of any debian based system (for now, I'll be adding support for rpm based systems and for freebsd later on), in my case, I'll be using `debian-live-12.1.0-amd64-standard.iso` to test it on.

```sh
cd ~/Downloads
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.1.0-amd64-cinnamon.iso
```
Once we have the ISO file, we can manipulate it with, `Ogun`. In order to __disassemble the file__ we use `-D` option and point to ISO file with `-i`.

```sh
cd ~/Projects/ogun/src
sudo ./ogun -D -i ~/Downloads/debian-live-12.1.0-amd64-standard.iso
```
The command will create folder named `iso` where the content of ISO and __squashfs__ file will be extracted.
After that we would like to configure the changes, that are configured in `confg.yaml`. __The configuration has structure which needs to be kept__.
The `config.yaml` is built upon lists of configuration, the first of them being `install`, that has sublist of packages that can be install with `apt` tool.
Packages listed  there will be installed, unless repositories are missing, then it will fail with appropriate error(TBD)

```sh
sudo ./ogun -S -v 12

```

## Tested ISO's

- Debian 12
- Debian 11
- Ubuntu 22.04


## Contributions

Any one is welcome to suggest anything they want to make this tool better. To do so please:

-  Clone the project
-  Make changes
      -  Keep in mind to stay tuned with best practices:
         -  At the moment the tool is developed with bash version 4+
         -  Take into consideration POSIX standard
         -  Use [shellcheck](https://www.shellcheck.net/) utility for code check, in vscode you have plugins
         -  Use [bats](https://bats-core.readthedocs.io/en/stable/) to write your tests
         -  Please consider to run tests to verify that you did not break anythin
         -  If you are adding features -> add new tests for your feature
         -  Use CI with `.gitlab-ci.yml` file to run and tests changes
         -  Always test on ISO's with suggested ISO's above, preferable to all the above

## Future plans

In future the tool will be rewritten witheither `Golang`, `Rust` or `C/C++`
